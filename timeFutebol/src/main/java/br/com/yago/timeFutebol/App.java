package br.com.yago.timeFutebol;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import br.com.yago.timeFutebol.resource.TimeRepository;

@RestController
@EnableAutoConfiguration
@SpringBootApplication
public class App implements CommandLineRunner
{
	
	//private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	TimeRepository repository;
	
	@RequestMapping("/")
	String home(){
		return "Olá mundo 11";
	}
	
	
    public static void main( String[] args )
    {
    	SpringApplication.run(App.class, args);
    }
    
    @Override
    public void run(String... args) throws Exception {
    	//logger.info("Time id 1 -> {}", repository.findById(1L).get().getNome());
    	
    	//repository.save(new Time(3L,"Fluminense", "Rosa"));
    	
    	//logger.info("Update 1 -> {}", repository.save(new Time(1L, "Vasco", "Roxo")));

    }
}
